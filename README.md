# Stack for samma.io

This will bring up a demo stack for you to test out the scanner.
The stack will deploy a fluentd collector that reads the logs files from the docker images and then sent over to a fluentd-shipper.
In the fluentd-shipper the logs are parsed and then sent over to Elasticsearch.

Kibana is then used to look at the logs 



### Fluentd
Fluentd is responsibel for colelcting the logs from the nodes and then parsing. This is done by first a fluentd deamonset that read the logs form the nodes.
The logs are then send ovre to a fluentd parser.
The parser is used to parse the logs into jason format and then sent over to elastic


### Elasticsearch
Here we store the data in the first verison the data is not persistant and only for dev (Larger stack is comming)

### Kibana
Kibana is the tool thar are used to search in the data and making grafs and dashbourd with



## Minikibe

To be able to run the stack in minikube please update memory on virtual machine to atleast 4G

The stack is build to work in minikube to test out the scanners and security system.
So the helm chart to apply the stack is configuerd to read logs from minikubes nodes.

To make it work for productions you need to update the elastic to work with more nodes.
And to add some config to the fluentd collector to read more logs.

In the fluentd-collector docker folder you can se more configs to work on more nodes type like EKS


### start up

This will deploy the stack into you kubernets cluster.

```
helm install stack    --debug samma-stack/
```

Then use 

```
minikube service kibana 
```

To browse to the kibana dasbourd.
At first you need to create index pattern for logstash* to view the logs.






#### Happy scanning